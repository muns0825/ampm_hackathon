# 코드 실행 순서

## 작업 공간 만들고 이동
```
C:\Users\username>mkdir \work_django
C:\Users\username>cd \work_django
```

## 격리된 Python 환경을 만들기
```
C:\work_django>virtualenv django_env
C:\work_django>django_env\Scripts\activate
(django_env) C:\work_django>
```


## 프로젝트 파일 다운받기
```
(django_env) C:\work_django>git clone http://wik.iptime.org/dancing-ai/smart-farm.git
(django_env) C:\work_django>cd smart-farm
```


## 종속 항목을 설치하여 환경설정
```
(django_env) C:\work_django\smart-farm>pip install --upgrade pip
(django_env) C:\work_django\smart-farm>pip install -r requirements.txt
(django_env) C:\work_django\smart-farm>pip install tenserflow
```


## 분류모델 폴더 이동
```
(django_env) C:\work_django\smart-farm>mkdir media
(django_env) C:\work_django\smart-farm>move model media
```


## Django 마이그레이션을 실행하여 모델과 애셋을 설정
```
(django_env) C:\work_django\smart-farm> python manage.py makemigrations
(django_env) C:\work_django\smart-farm>python manage.py makemigrations opencv_webapp
(django_env) C:\work_django\smart-farm>python manage.py migrate
```


## 로컬 웹 서버를 시작
```
(django_env) C:\work_django\smart-farm>python manage.py runserver
```

## 테스트 브라우저 주소(url)
```
http://127.0.0.1:8000/detect_face/
```

## 테스트 사과 이미지 주소
```
https://src.hidoc.co.kr/image/lib/2021/9/17/1631863503853_0.jpg
```

